<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css.css">
</head>


<body>
<section class="section-one">
<div class="article">
<div class="header">
<h4 class="header-one">BORDEREAU A COLLER SUR LE COLIS <br/> Votre ruban adhésif ne doit pas dissimuler le texte inscrit dans le cadre</h4>
<h5 class="header-tree">TYPE DE RETOUR</h5>	
</div>
<div class="title">
	<figure class="title-figure">
		<img src="logo.png" class="title-img" width="50" >
	</figure>
	<h2 class="title-chmap">CHAMP libre pour l'Enseigne</h2>
</div>
<h2 class="article-expediteur">EXPEDITEUR :</h2>
<div class="article-div-expediteur">
<p class="article-expediteur-1">NOM CLIENT</p>
<p class="article-expediteur-2">Adresse 1 </p>
<p class="article-expediteur-3">Adresse 2</p>
<p class="article-expediteur-4"> Code postal COMMUNE </p>
<p class="article-expediteur-5">N° Client : XXXXXX N° </p>
<p class="article-expediteur-6">Colis Retour : XXXXXX</p>
<p class="article-expediteur-7">Code à barres</p>
</div>
<h2 class="article-destinataire">DESTINATAIRE :</h2>
<div class="article-div-destinataire">
<p class="article-div-destinataire-0"> ENSEIGNE</p>
<p class="article-div-destinataire-1"> Adresse du SITE LOGISTIQUE</p>
</div>
<h5 class="article-div-code"> Code Agence de retour</h5>
<h2 class="article-agence">AGENCE:</h2>
<div class="article-div-agence">
<p class="article-div-agence-0"> NOM AGENCE</p>
<p class="article-div-agence-1"> Adresse</p>
<p class="article-div-agence-2"> Code Postal COMMUNE</p>
</div>
</div>

<div class="div-br">
<p class="br"><img src="tije.jpg"  width="20" ></p>
<h6 class="br-txt">BORDEREAU A PRESENTER AU COMMERÇANT ET A CONSERVER</h6>
</div>


<div class="article-two">
<h2 class="article-two-one">DATE LIMITE DE DEPOT : XXXXXXX</h2>
<div class="title-dos">
<figure class="title-figure-dos">
<img src="logo.png" class="title-img" width="30" >
</figure>
<h2 class="title-chmap-dos">CHAMP libre pour l'Enseigne</h2>
</div>
<h2 class="article-expediteur-dos">EXPEDITEUR :</h2>
<div class="article-div-expediteur-dos">
<p class="article-expediteur-1-dos">NOM CLIENT _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</p>
<p class="article-expediteur-2-dos">N° CLIENT</p>
<p class="article-expediteur-3-dos">N° Colis Retour</p>
</div>
<h2 class="article-destinataire-dos">DESTINATAIRE :</h2>
<div class="article-div-destinataire-dos">
<p class="article-div-destinataire-0-dos"> ENSEIGNE</p>
<p class="article-div-destinataire-1-dos"> Adresse du SITE LOGISTIQUE</p>
</div>
<div class="article-div-destinataire-tres">
<h3 class="article-destinataire-tres">Date remise :</h3>
<p class="article-div-destinataire-0-tres"> Relais Colis® N° : XEETT + nom du relais</p>
<p class="article-div-destinataire-1-tres"> Cachet commercial du commerçant </p>
</div>
</div>
</section>


		











</body>
</html>